﻿Imports System.Collections.Specialized
Imports System.Windows.Controls
Imports System.Windows.Data
Imports Microsoft.LightSwitch.Threading
Imports System.ComponentModel
Imports System.Runtime.InteropServices.Automation

Namespace LightSwitchApplication

    Public Class CreateNewTenantEdit

        Private _grid As IContentItemProxy
        Dim TenantPayment As TenantPayment

        Private Sub CreateNewTenantEdit_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))

            ' Open Tenant details if we are referencing an existing record.
            If Me.TenantID.HasValue Then
                Me.TenantHeaderProperty = Me.TenantHeader
            Else
                'Else open a new Tenant Record
                Me.TenantHeaderProperty = New TenantHeader()
                Me.FindControl("TenantStatement").IsVisible = False

            End If

        End Sub

        Private Sub CreateNewTenantEdit_Created()
            _grid = FindControl("TenantPayments")
            AddHandler _grid.ControlAvailable, AddressOf TenantPayment_Available
        End Sub
        Private Sub TenantPayment_Available(sender As Object, e As ControlAvailableEventArgs)

            Dim view As PagedCollectionView = New PagedCollectionView(CType(e.Control, DataGrid).ItemsSource)

            Using view.DeferRefresh()
                view.GroupDescriptions.Clear()

                ' What do you want to order by
                view.SortDescriptions.Add(New SortDescription("DatePaid", ListSortDirection.Ascending))

                ' What you want to group by
                view.GroupDescriptions.Add(New PropertyGroupDescription("DatePaid"))
            End Using

            CType(e.Control, DataGrid).ItemsSource = view

        End Sub
        Private Sub TenantDetails_SelectionChanged()
            Dim selectedItem = Me.TenantDetails.SelectedItem
            If (selectedItem Is Nothing) Then Return

            Dispatchers.Main.BeginInvoke(
                Sub()
                    RemoveHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf TenantPropertyDetail_Changed
                    RemoveHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf TenantPropertyDetail_Changed
                    AddHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf TenantPropertyDetail_Changed

                End Sub
                )

        End Sub
        Private Sub TenantPropertyDetail_Changed(sender As Object, e As PropertyChangedEventArgs)

            Select Case e.PropertyName

                Case "PropertyType"

                    If Me.TenantDetails.SelectedItem.PropertyType IsNot Nothing Then
                        If Me.TenantDetails.SelectedItem.PropertyType.Equals("house") Then

                            Try
                                Me.Details.Dispatcher.BeginInvoke(
                                    Sub()
                                    Me.OpenModalWindow("SelectedHouse")
                                End Sub)

                                'Me.TenantDetails.SelectedItem.PropertyToLet = SelectedHouse.Summary

                            Catch ex As Exception
                                Me.ShowMessageBox(ex.Message, "Error Occurred", MessageBoxOption.Ok)
                            End Try
                        End If

                    ElseIf Me.TenantDetails.SelectedItem.PropertyType IsNot Nothing Then
                        If Me.TenantDetails.SelectedItem.PropertyType = "office" Then
                            Try
                                Me.Details.Dispatcher.BeginInvoke(
                                    Sub()
                                    Me.OpenModalWindow("SelectedOffice")
                                End Sub)

                                'Me.TenantDetails.SelectedItem.PropertyToLet = SelectedOffice.Summary

                            Catch ex As Exception
                                Me.ShowMessageBox(ex.Message, "Error Occurred", MessageBoxOption.Ok)
                            End Try
                        End If
                    End If
                Case "ExiDate"

                    If Me.TenantDetails.SelectedItem.ExitDate IsNot Nothing Then
                        Try
                            'Set the selected property to vacant
                            If Me.TenantDetails.SelectedItem.ExitDate IsNot Nothing Then

                                ShowMessageBox("A date on which to set the property occupied to vacant has been set.")

                            End If
                        Catch ex As Exception
                            Me.ShowMessageBox(ex.Message, "Error Occurred", MessageBoxOption.Ok)
                        End Try
                    End If

            End Select

        End Sub
        Private Sub CreateNewTenantEdit_Saved()
            ' On Saving re-open the same record.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.TenantHeaderProperty)
        End Sub

        Private Sub SelectedHouse_Changed()

            'Set the name of the selected property to let
            Me.TenantDetails.SelectedItem.PropertyToLet = Me.SelectedHouse.Summary.ToString()

            'Copy property to let for reference
            Me.TenantDetails.SelectedItem.PropertyID = Me.SelectedHouse.Id

            'set the selected property as occupied
            Me.SelectedHouse.Availability = "occupied"

            'add rent to the property
            Me.TenantDetails.SelectedItem.RentPayable = Me.SelectedHouse.Rent

            'Set the initial rent due
            If (Me.TenantDetails.SelectedItem.Details.Properties.RentDue.Value = 0) Then

                Me.TenantDetails.SelectedItem.RentDue = Me.SelectedHouse.Rent

            End If

        End Sub

        Private Sub TenantDetails_Loaded(succeeded As Boolean)

            Dim dw = Me.DataWorkspace.ApplicationData
            Dim currentDetail = dw.TenantDetails.Where(Function(det) det.PropertyToLet IsNot Nothing).Execute()

        End Sub

        Private Sub CreateNewTenantEdit_Saving(ByRef handled As Boolean)
            ' Write your code here.

        End Sub

        Private Sub CreateNewTenantEdit_SaveError(exception As Exception, ByRef handled As Boolean)
            ' Un-delete deleted records that had server-side validation errors 
            ' while trying to delete tenant property to let details
            Dim validationExc = TryCast(exception, ValidationException)
            If validationExc IsNot Nothing Then
                Dim entities = From v In validationExc.ValidationResults
                Let e = TryCast(v.Target, IEntityObject)
                Where e IsNot Nothing AndAlso
                e.Details.EntityState = EntityState.Deleted
                Select e
                For Each e In entities
                    e.Details.DiscardChanges()
                Next
            End If
        End Sub

        Private Sub TenantPaymentsAddAndEditNew_Execute()

            ' Write your code here.
            NewPayment = TenantPayments.AddNew()
            Me.OpenModalWindow("AddNewPayment")

            'Disable some of the control as the infor required is depedent on the mode
            Me.FindControl("eMoneyPhoneNumber2").IsEnabled = False
            Me.FindControl("eMoneyCode1").IsEnabled = False
            Me.FindControl("ChequeNo1").IsEnabled = False
            Me.FindControl("BankSlipCode1").IsEnabled = False
            Me.FindControl("Bank2").IsEnabled = False

            'Set up a listener for the mode changed
            Dim control = Me.FindControl("Mode1")
            AddHandler control.ControlAvailable, AddressOf NewPaymentMode_Changed

        End Sub

        Private Sub NewPaymentMode_Changed(sender As Object, e As ControlAvailableEventArgs)

            RemoveHandler CType(e.Control, System.Windows.Controls.AutoCompleteBox).SelectionChanged, AddressOf ModeSelectionChanged
            RemoveHandler CType(e.Control, System.Windows.Controls.AutoCompleteBox).SelectionChanged, AddressOf ModeSelectionChanged
            AddHandler CType(e.Control, System.Windows.Controls.AutoCompleteBox).SelectionChanged, AddressOf ModeSelectionChanged

            'Set up events to detect that the user has chosen a certain mode

        End Sub

        Private Sub ModeSelectionChanged(sender As Object, e As System.Windows.RoutedEventArgs)

            Dim combobox = CType(sender, System.Windows.Controls.AutoCompleteBox)

            'Enable other payment detail options based on the mode of payment selected
            If combobox.Text.Equals("Mobile E-Money") Then
                Me.FindControl("eMoneyPhoneNumber2").IsEnabled = True
                Me.FindControl("eMoneyCode1").IsEnabled = True
                Me.FindControl("ChequeNo1").IsEnabled = False
                Me.FindControl("BankSlipCode1").IsEnabled = False
                Me.FindControl("Bank2").IsEnabled = False

            ElseIf combobox.Text.Equals("Cheque") Then
                Me.FindControl("eMoneyPhoneNumber2").IsEnabled = False
                Me.FindControl("eMoneyCode1").IsEnabled = False
                Me.FindControl("ChequeNo1").IsEnabled = True
                Me.FindControl("BankSlipCode1").IsEnabled = False
                Me.FindControl("Bank2").IsEnabled = False

            ElseIf combobox.Text.Equals("Credit") Then
                Me.FindControl("eMoneyPhoneNumber2").IsEnabled = False
                Me.FindControl("eMoneyCode1").IsEnabled = False
                Me.FindControl("ChequeNo1").IsEnabled = False
                Me.FindControl("BankSlipCode1").IsEnabled = False
                Me.FindControl("Bank2").IsEnabled = False

            ElseIf combobox.Text.Equals("Bank Deposit") Then
                Me.FindControl("eMoneyPhoneNumber2").IsEnabled = False
                Me.FindControl("eMoneyCode1").IsEnabled = False
                Me.FindControl("ChequeNo1").IsEnabled = False
                Me.FindControl("BankSlipCode1").IsEnabled = True
                Me.FindControl("Bank2").IsEnabled = True

            End If

        End Sub

        Private Sub SavePayment_Execute()

            ' Close Modal Window and save the new payment.
            Me.CloseModalWindow("AddNewPayment")
            Me.TenantDetails.SelectedItem.RentDue = Me.TenantDetails.SelectedItem.RentDue - Me.NewPayment.Amount
            Me.Save()

        End Sub

        Private Sub Cancel_Execute()

            ' Discard the new payment
            Me.NewPayment = Nothing
            Me.CloseModalWindow("AddNewPayment")

        End Sub

        Private Sub TenantPaymentsAddAndEditNew_CanExecute(ByRef result As Boolean)
            ' Write your code here.

        End Sub

        Private Sub sendsms_Execute()
            ' Write your code here.
            'SMSSender.sendSMS("from vb lightswitch")
        End Sub



        Private Sub UploadFile_Execute()
            ' Write your code here.

            Dispatchers.Main.Invoke(
                Sub()
                    Dim openDialog As New Controls.OpenFileDialog
                    openDialog.Filter = "All files|*.*"
                    'Use this syntax to only allow Word/Excel files
                    'openDialog.Filter = "Word Files|*.doc|Excel Files |*.xls"
                    openDialog.Filter = "PDF Files|*.pdf"
                    If openDialog.ShowDialog = True Then
                        Using fileData As System.IO.FileStream = openDialog.File.OpenRead
                            Dim fileLen As Long = fileData.Length
                            If (fileLen > 0) Then
                                Dim fileBArray(fileLen - 1) As Byte
                                fileData.Read(fileBArray, 0, fileLen)
                                fileData.Close()
                                Me.TenantDetails.SelectedItem.TenantFile.Document = fileBArray
                                'Me.PropertyHeader.PropertyFiles.Extension = openDialog.File.Extension.ToString()
                                Me.TenantDetails.SelectedItem.TenantFile.FileName = openDialog.File.Name
                            End If
                        End Using
                    End If
                End Sub)
        End Sub

        Private Sub DownloadFile_Execute()
            ' Write your code here.
            Dispatchers.Main.Invoke(
                Sub()
                    'Replace ProductDocument with the name of your entity
                    Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(TenantDetails.SelectedItem.TenantFile.FileName)
                    Dispatchers.Main.Invoke(
                        Sub()
                            Dim saveDialog As New Controls.SaveFileDialog
                            If saveDialog.ShowDialog = True Then
                                Using fileStream As Stream = saveDialog.OpenFile
                                    ms.WriteTo(fileStream)
                                End Using
                            End If
                        End Sub)
                End Sub)
        End Sub

        Private Sub DeleteFile_Execute()
            ' Write your code here.
            Me.TenantFiles.DeleteSelected()
        End Sub
        Private Sub TenantStatement_Execute()

            'Write your code here.
            Dim status = MyReports.RunCustomerReportDynamicTemplate(Me.TenantDetails.SelectedItem, Me.DataWorkspace)

            If Not (MyReports.RunCustomerReportDynamicTemplate(Me.TenantDetails.SelectedItem, Me.DataWorkspace).Equals("Done")) Then
                'get the error and show it
                Me.ShowMessageBox(status.ToString, "Error Occurred", MessageBoxOption.Ok)

            End If

            If AutomationFactory.IsAvailable Then
                Try


                    Dim resourceInfo = System.Windows.Application.GetResourceStream(New Uri("Resources/LetterTemplate.dotx", UriKind.Relative))
                    Dim path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "LetterTemplate.dotx"
                    Dim file = System.IO.File.Create(path)
                    file.Close()
                    'Write the stream to the file
                    Dim stream As System.IO.Stream = resourceInfo.Stream
                    Using fileStream = System.IO.File.Open(path, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write, System.IO.FileShare.None)
                        Dim buffer(0 To stream.Length - 1) As Byte
                        stream.Read(buffer, 0, stream.Length)
                        fileStream.Write(buffer, 0, buffer.Length)
                    End Using

                    Using wordApp = AutomationFactory.CreateObject("Word.Application")

                        ' Look in the ClientGenerated project to view the Word template.
                        Dim wordDoc = wordApp.Documents.Open("ResourceTenantStatement.dotx")

                        wordDoc.Select()
                        'wordApp.Visible = True


                        ' Create a new file rather than open it from a template
                        'Dim wordDoc = wordApp.Documents.Add()
                        'Dim wordSelection As Object
                        'wordSelection = wordApp.Selection

                        ' Create a header paragraph.
                        Dim para = wordDoc.Paragraphs.Add()

                        para.Range.Text = "Tenant Payment Statement"
                        para.Range.Text = "TenantName: " + Me.TenantHeaderProperty.FullName
                        para.Range.Text = "IDNumber: " + Me.TenantHeaderProperty.IdNumber
                        para.Range.Text = "PhoneNumber: " + Me.TenantHeaderProperty.PhoneNumber
                        para.Range.Text = "TenantEmail: " + Me.TenantHeaderProperty.Email
                        Dim currentProperty = Me.TenantDetails.SelectedItem.PropertyToLet
                        para.Range.Text = "PropertyName: " + currentProperty

                        para.Range.Style = "Heading 2"
                        para.Range.InsertParagraphAfter()

                        ' Save the current font and start using Courier New.
                        Dim old_font As String = para.Range.Font.Name
                        para.Range.Font.Name = "Courier New"

                        ' Start a new paragraph and then switch back to the
                        ' original font.
                        'para.Range.InsertParagraphAfter()
                        'para.Range.Font.Name = old_font

                        'wordDoc.Close(0)
                        'wordApp.Quit()
                    End Using
                Catch ex As InvalidOperationException
                    Me.ShowMessageBox(ex.Message, "Error Occurred", MessageBoxOption.Ok)
                End Try
            End If
        End Sub

    End Class

End Namespace