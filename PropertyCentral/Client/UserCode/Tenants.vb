﻿Imports System.Collections.Specialized
Imports System.Windows.Controls
Imports Microsoft.LightSwitch.Threading


Namespace LightSwitchApplication

    Public Class Tenants

        Private Sub gridAddNew_CanExecute(ByRef result As Boolean)
            ' Write your code here.

        End Sub

        Private Sub gridAddNew_Execute()
            ' Write your code here.
            Me.Application.ShowCreateNewTenantEdit(Nothing)
        End Sub

        Private Sub gridEditSelected_CanExecute(ByRef result As Boolean)
            ' Write your code here.

        End Sub

        Private Sub gridEditSelected_Execute()
            ' Write your code here.
            Me.Application.ShowCreateNewTenantEdit(Me.TenantHeaders.SelectedItem.Id)
        End Sub

    End Class

End Namespace
