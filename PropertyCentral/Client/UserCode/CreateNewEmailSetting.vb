﻿
Namespace LightSwitchApplication

    Public Class CreateNewEmailSetting

        Private Sub CreateNewEmailSetting_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.

            Me.EmailSettingProperty = New EmailSetting()
        End Sub

        Private Sub CreateNewEmailSetting_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.EmailSettingProperty)
        End Sub

    End Class

End Namespace