﻿
Namespace LightSwitchApplication

    Public Class CreateNewInsuranceDetail

        Private Sub CreateNewInsuranceDetail_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.InsuranceHeaderProperty = New InsuranceHeader()
        End Sub

        Private Sub CreateNewInsuranceDetail_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.InsuranceHeaderProperty)
        End Sub

    End Class

End Namespace