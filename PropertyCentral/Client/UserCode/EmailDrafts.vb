﻿
Namespace LightSwitchApplication

    Public Class EmailDrafts

        Private Sub AddNewDraft_Execute()
            ' Write your code here.
            EmailDrafts.AddNew()
            Me.OpenModalWindow("AddEditDraft")
        End Sub

        Private Sub EditDraft_Execute()
            ' Write your code here.
            Me.OpenModalWindow("AddEditDraft")
        End Sub

        Private Sub AddDraft_Execute()
            ' Write your code here.
            Me.CloseModalWindow("AddEditDraft")
        End Sub

        Private Sub Cancel_Execute()
            ' Write your code here.
            CType(EmailDrafts.SelectedItem, EmailDraft).Details.DiscardChanges()
            Me.CloseModalWindow("AddEditDraft")
        End Sub

        Private Sub DeleteDraft_Execute()
            ' Write your code here.
            EmailDrafts.SelectedItem.Delete()
        End Sub
    End Class

End Namespace
