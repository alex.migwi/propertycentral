﻿
Namespace LightSwitchApplication

    Public Class LandLordDetail

        Private Sub LandLord_Loaded(succeeded As Boolean)
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.LandLord)
        End Sub

        Private Sub LandLord_Changed()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.LandLord)
        End Sub

        Private Sub LandLordDetail_Saved()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.LandLord)
        End Sub

    End Class

End Namespace