﻿
Namespace LightSwitchApplication

    Public Class CreateNewLandLord

        Private Sub CreateNewLandLord_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.LandLordProperty = New LandLord()
        End Sub

        Private Sub CreateNewLandLord_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.LandLordProperty)
        End Sub

    End Class

End Namespace