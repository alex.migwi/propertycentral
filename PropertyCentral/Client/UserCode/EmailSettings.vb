﻿
Namespace LightSwitchApplication

    Public Class EmailSettings

        Private Sub EmailSettings_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.EmailSettingProperty = DataWorkspace.ApplicationData.EmailSettings.FirstOrDefault()
            If EmailSettingProperty Is Nothing Then
                EmailSettingProperty = New EmailSetting
            End If
        End Sub
    End Class

End Namespace
