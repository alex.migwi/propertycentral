﻿
Namespace LightSwitchApplication

    Public Class CreateNewLandLordEdit

        Private Sub CreateNewLandLordEdit_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here.

            If LandlordID.HasValue Then
                Me.LandlordProperty = Me.Landlord
            Else
                Me.LandlordProperty = New Landlord()
            End If

        End Sub

        Private Sub CreateNewLandLordEdit_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.LandLordProperty)
        End Sub

        Private Sub PropertyHeadersAddAndEditNew_CanExecute(ByRef result As Boolean)
            ' Write your code here.

        End Sub

        Private Sub PropertyHeadersAddAndEditNew_Execute()
            ' Write your code here.

        End Sub

        Private Sub PropertyHeadersEditSelected_CanExecute(ByRef result As Boolean)
            ' Write your code here.

        End Sub

        Private Sub PropertyHeadersEditSelected_Execute()
            ' Write your code here.

        End Sub
    End Class

End Namespace