﻿
Imports System.Collections.Specialized
Imports System.Windows.Controls
Imports Microsoft.LightSwitch.Threading

Namespace LightSwitchApplication


    Public Class CreateNewPropertyEdit

        Private Sub CreateNewPropertyEdit_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here. 

            'Code from How to monitor INotifyPropertyChanged on a details type screen? 

            If Me.PropertyID.HasValue Then
                Me.PropertyHeaderProperty = Me.PropertyHeader
                Me.PropertyID2 = Me.PropertyID

                If Me.PropertyHeaderProperty.PropertyType.Equals("res-house") Then

                    FindControl("PropertyDetailHouses").IsVisible = True
                    FindControl("PropertyDetailHouses").Show()

                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = False

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("res-plot") Then

                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = True
                    FindControl("PropertyDetailLands").Show()

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("com-off") Then


                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = True
                    FindControl("PropertyDetailOffice").Show()
                    FindControl("PropertyDetailLands").IsVisible = False

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("com-plot") Then

                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = True
                    FindControl("PropertyDetailLands").Show()
                End If
            Else
                Me.PropertyHeaderProperty = New PropertyHeader()
            End If
        End Sub

        Private Sub CreateNewPropertyEdit_Created()

            Microsoft.LightSwitch.Threading.Dispatchers.Main.BeginInvoke(
               Sub()
                   AddHandler DirectCast(Me.PropertyHeaderProperty, INotifyPropertyChanged).PropertyChanged, AddressOf PropertyType_Changed
               End Sub
           )

            If Not Me.PropertyID.HasValue Then
                FindControl("PropertyDetailHouses").IsVisible = False
                FindControl("PropertyDetailOffice").IsVisible = False
                FindControl("PropertyDetailLands").IsVisible = False
            End If

        End Sub

        Private Sub PropertyType_Changed(sender As Object, e As PropertyChangedEventArgs)

            If e.PropertyName = "PropertyType" Then
                If Me.PropertyHeaderProperty.PropertyType.Equals("res-house") Then

                    FindControl("PropertyDetailHouses").IsVisible = True
                    FindControl("PropertyDetailHouses").Show()

                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = False

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("res-plot") Then

                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = True
                    FindControl("PropertyDetailLands").Show()

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("com-off") Then

                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = True
                    FindControl("PropertyDetailOffice").Show()
                    FindControl("PropertyDetailLands").IsVisible = False

                ElseIf Me.PropertyHeaderProperty.PropertyType.Equals("com-plot") Then

                    FindControl("PropertyDetailHouses").IsVisible = False
                    FindControl("PropertyDetailOffice").IsVisible = False
                    FindControl("PropertyDetailLands").IsVisible = True
                    FindControl("PropertyDetailLands").Show()
                End If
            End If

        End Sub
        Private Sub CreateNewPropertyEdit_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.PropertyHeaderProperty)
        End Sub

        Private Sub InsuaranceHeaderAddNew_Execute()
            ' Write your code here.
            Me.Application.ShowCreateNewInsuranceEdit(Nothing)
        End Sub

        Private Sub InsuaranceHeaderEditSelected_Execute()
            ' Write your code here.
            Me.Application.ShowCreateNewInsuranceEdit(Me.InsuaranceHeader.SelectedItem.Id)
        End Sub

        Private Sub UploadFile_Execute()
            ' Write your code here.

            Dispatchers.Main.Invoke(
                Sub()
                    Dim openDialog As New Controls.OpenFileDialog
                    openDialog.Filter = "All files|*.*"
                    'Use this syntax to only allow Word/Excel files
                    'openDialog.Filter = "Word Files|*.doc|Excel Files |*.xls"
                    openDialog.Filter = "PDF Files|*.pdf"
                    If openDialog.ShowDialog = True Then
                        Using fileData As System.IO.FileStream =
                        openDialog.File.OpenRead
                            Dim fileLen As Long = fileData.Length
                            If (fileLen > 0) Then
                                Dim fileBArray(fileLen - 1) As Byte
                                fileData.Read(fileBArray, 0, fileLen)
                                fileData.Close()
                                Me.PropertyHeader.PropertyFiles.Document = fileBArray
                                'Me.PropertyHeader.PropertyFiles.Extension = openDialog.File.Extension.ToString()
                                Me.PropertyHeader.PropertyFiles.Filename = openDialog.File.Name
                            End If
                        End Using
                    End If
                End Sub)
        End Sub

        Private Sub DownloadFile_Execute()
            ' Write your code here.
            Dispatchers.Main.Invoke(
                Sub()
                    'Replace ProductDocument with the name of your entity
                    Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(PropertyHeader.PropertyFiles.Filename)
                    Dispatchers.Main.Invoke(
                        Sub()
                            Dim saveDialog As New Controls.SaveFileDialog
                            If saveDialog.ShowDialog = True Then
                                Using fileStream As Stream = saveDialog.OpenFile
                                    ms.WriteTo(fileStream)
                                End Using
                            End If
                        End Sub)
                End Sub)
        End Sub

        Private Sub DeleteFile_Execute()
            ' Write your code here.
            Me.PropertyFiles.DeleteSelected()
        End Sub

        Private Sub PropertyDetailHouses_SelectionChanged()
            Dim selectedItem = Me.PropertyDetailHouses.SelectedItem
            If (selectedItem Is Nothing) Then Return

            Dispatchers.Main.BeginInvoke(
                Sub()
                    'RemoveHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf TenantPropertyDetail_Changed
                    'RemoveHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf TenantPropertyDetail_Changed
                    AddHandler DirectCast(selectedItem, INotifyPropertyChanged).PropertyChanged, AddressOf PropertyFromDateDetail_Changed

                End Sub
                )
        End Sub

        Private Sub PropertyFromDateDetail_Changed(sender As Object, e As PropertyChangedEventArgs)

            Select Case e.PropertyName

                Case "FromDate"

                    If Me.PropertyDetailHouses.SelectedItem.FromDate IsNot Nothing Then

                        Try
                            Me.PropertyDetailHouses.SelectedItem.Availability = "vacant"

                        Catch ex As Exception
                            Me.ShowMessageBox(ex.Message, "Error Occurred", MessageBoxOption.Ok)
                        End Try

                    End If

            End Select

        End Sub

    End Class

End Namespace
