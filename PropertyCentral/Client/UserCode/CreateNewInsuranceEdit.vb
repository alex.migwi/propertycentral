﻿
Namespace LightSwitchApplication

    Public Class CreateNewInsuranceEdit

        Private Sub CreateNewInsuranceEdit_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here.

            If Me.InsuranceID.HasValue Then
                Me.InsuranceHeaderProperty = Me.InsuranceHeader
            Else
                Me.InsuranceHeaderProperty = New InsuranceHeader()
            End If

        End Sub

        Private Sub CreateNewInsuranceEdit_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.InsuranceHeaderProperty)
        End Sub

    End Class

End Namespace