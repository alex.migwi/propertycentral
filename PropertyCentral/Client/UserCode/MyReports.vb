﻿Imports System.Runtime.InteropServices.Automation
Imports System.Windows
Imports System.Windows.Interop

Namespace LightSwitchApplication

    Module MyReports

        Public Function RunCustomerReportDynamicTemplate(ByRef tenant As TenantDetail, ByRef dw As DataWorkspace)
            Dim Status As String = "Failed"
            If AutomationFactory.IsAvailable Then

                Try

                    Dim resourceInfo = System.Windows.Application.GetResourceStream(
                                    New Uri("TenantStatement.dotx", UriKind.Relative))

                    Dim fileName = CopyStreamToTempFile(resourceInfo.Stream, ".dotx")


                    Using word = AutomationFactory.CreateObject("Word.Application")

                        Dim doc = word.Documents.Open(fileName)


                        doc.Select()
                        'wordApp.Visible = True


                        ' Create a new file rather than open it from a template
                        'Dim wordDoc = wordApp.Documents.Add()
                        'Dim wordSelection As Object
                        'wordSelection = wordApp.Selection

                        ' Create a header paragraph.
                        Dim para = doc.Paragraphs.Add()

                        para.Range.Text = "Tenant Payment Statement"
                        para.Range.Style = "Heading 2"
                        para.Range.InsertParagraphAfter()
                        para.Range.Text = "Tenant Name: " + tenant.TenantHeader.FullName
                        para.Range.Style = "Heading 2"
                        para.Range.InsertParagraphAfter()
                        para.Range.Text = "Property Name: " + tenant.PropertyToLet
                        para.Range.Style = "Heading 2"
                        para.Range.InsertParagraphAfter()

                        ' Save the current font and start using Courier New.
                        Dim old_font As String = para.Range.Font.Name
                        para.Range.Font.Name = "Courier New"

                        InsertHeaderAndFooter(doc, word)

                        word.Visible = True

                        Status = "Done"
                    End Using
                Catch ex As Exception
                    Dim messageRaised = dw.ApplicationData.OnScreenMessages.AddNew()
                    messageRaised.Message = ex.ToString
                    dw.ApplicationData.SaveChanges()
                    'LightSwitchApplication.OnScreenMessages.showErrorMessageBox(New InvalidOperationException("Failed to create customer report.", ex))
                    Status = "Failed"

                End Try
            End If

            Return Status

        End Function

        Private Function CopyStreamToTempFile(ByVal stream As System.IO.Stream, ByVal ext As String) As String
            Dim path = GetTempFileName(ext)
            'Create the temp file
            Dim file = System.IO.File.Create(path)
            file.Close()
            'Write the stream to disk
            Using fileStream = System.IO.File.Open(path,
                                                   System.IO.FileMode.OpenOrCreate,
                                                   System.IO.FileAccess.Write,
                                                   System.IO.FileShare.None)

                Dim buffer(0 To stream.Length - 1) As Byte
                stream.Read(buffer, 0, stream.Length)
                fileStream.Write(buffer, 0, buffer.Length)

                fileStream.Close()
            End Using
            Return path
        End Function

        Private Function GetTempFileName(ByVal ext As String) As String
            'Return a uinuqe file name in My Documents\Reports based on a guid
            Dim path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Reports"
            If Not Directory.Exists(path) Then
                Directory.CreateDirectory(path)
            End If

            Dim filename = Guid.NewGuid().ToString() & ext
            path = System.IO.Path.Combine(path, filename)

            Return path
        End Function

        Private Sub InsertHeaderAndFooter(ByRef oDoc As Object, ByVal word As Object)

            Dim oHeader = oDoc.Headers.Add()

            oHeader.Range.Text = "Tenant Payment Statement"
            oHeader.Range.Style = "Heading 2"

        End Sub
    End Module

End Namespace