﻿
Namespace LightSwitchApplication

    Public Class CreateNewSupplierEdit

        Private Sub CreateNewSupplierEdit_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.

            If Me.SupplierID.HasValue Then
                Me.SupplierProperty = Me.Supplier
            Else
                Me.SupplierProperty = New Supplier()
            End If

        End Sub

        Private Sub CreateNewSupplierEdit_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.SupplierProperty)
        End Sub

    End Class

End Namespace