﻿
Namespace LightSwitchApplication

    Public Class CreateNewEditAgencyDetail

        Private Sub CreateNewEditAgencyDetail_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.AgencyDetailProperty = DataWorkspace.ApplicationData.AgencyDetails.FirstOrDefault()

            If AgencyDetailProperty Is Nothing Then
                AgencyDetailProperty = New AgencyDetail
            End If

            'Me.AgencyDetailProperty = New AgencyDetail()
        End Sub

        Private Sub CreateNewEditAgencyDetail_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.AgencyDetailProperty)
        End Sub

    End Class

End Namespace