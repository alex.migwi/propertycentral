﻿Imports System.IO.Ports
Imports System.IO

Namespace LightSwitchApplication

    Module SMSSender

        Dim SerialPort As New SerialPort()
        Dim CR As String

        Public Function sendSMS(message As String)

            Dim status As Boolean = False

            If SerialPort.IsOpen Then
                SerialPort.Close()
            End If
            SerialPort.PortName = "COM19"
            SerialPort.BaudRate = 9600
            SerialPort.Parity = Parity.None
            SerialPort.StopBits = StopBits.One
            SerialPort.DataBits = 8
            SerialPort.Handshake = Handshake.RequestToSend
            SerialPort.DtrEnable = True
            SerialPort.RtsEnable = True
            SerialPort.NewLine = vbCrLf
            SerialPort.Open()
            If SerialPort.IsOpen() Then
                SerialPort.Write("AT" & vbCrLf)
                SerialPort.Write("AT+CMGF=1" & vbCrLf)
                SerialPort.Write("AT+CMGS=" & Chr(34) & +254724866771 & Chr(34) & vbCrLf)
                SerialPort.Write(message & Chr(26))
            Else
                MsgBox("Port not available")
            End If

            Return status
        End Function

    End Module
End Namespace

