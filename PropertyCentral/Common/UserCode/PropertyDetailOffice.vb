﻿
Namespace LightSwitchApplication

    Public Class PropertyDetailOffice

        Private Sub Rent_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            result = Me.ChargePerSqm * NoOfSqm
        End Sub

        Private Sub Summary_Compute(ByRef result As String)
            ' Set result to the desired field value
            result = Me.OfficeNo + " - " + Me.PropertyHeader.PropertyName
        End Sub

        Private Sub PropertyDetailOffice_Created()
            Me.Availabilty = "vacant"
        End Sub
    End Class

End Namespace
