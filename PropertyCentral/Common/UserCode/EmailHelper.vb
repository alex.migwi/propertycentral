﻿Imports System.Net
Imports System.Net.Mail
Imports System.IO
Imports LightSwitchApplication.DataWorkspace

Namespace LightSwitchApplication

    Public Module EmailHelper

        'Create a dataworkspace
        Dim dw = D

        Const SMTPServer As String = "relay.yourmailserver.net"
        Const SMTPUserId As String = "myUsername"
        Const SMTPPassword As String = "myPassword"
        Const SMTPPort As Integer = 25

        Public Sub SendMail(sendFrom As String, sendTo As String, subject As String,
        body As String,
        attachment As Byte(),
        filename As String)
            Dim fromAddress As New MailAddress(sendFrom)
            Dim toAddress As New MailAddress(sendTo)
            Dim mail As New MailMessage()
            mail.From = fromAddress
            mail.To.Add(toAddress)
            mail.Subject = subject
            mail.Body = body
            If body.ToLower().Contains("<html>") Then
                mail.IsBodyHtml = True
            End If
            Dim smtp As New SmtpClient(SMTPServer, SMTPPort)
            If attachment IsNot Nothing AndAlso Not String.IsNullOrEmpty(filename) Then
                Using ms As New MemoryStream(attachment)
                    mail.Attachments.Add(New Attachment(ms, filename))
                    smtp.Send(mail)
                End Using
            Else
                smtp.Send(mail)
            End If
        End Sub

    End Module

End Namespace
