﻿
Namespace LightSwitchApplication

    Public Class PropertyDetailLand

        Private Sub Summary_Compute(ByRef result As String)
            ' Set result to the desired field value
            result = Me.LandRefNo + " - " + Me.Acreage + " Acres - " + Me.PropertyHeader.Location
        End Sub

        Private Sub PropertyDetailLand_Created()

            Me.Availability = "available"

        End Sub
    End Class

End Namespace
