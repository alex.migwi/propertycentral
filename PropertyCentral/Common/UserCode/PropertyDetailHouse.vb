﻿
Namespace LightSwitchApplication

    Public Class PropertyDetailHouse

        Private Sub PropertyDetailHouse_Created()
            Me.Bathrooms = 0
            Me.Bedrooms = 1
            Me.Availability = "commited"
        End Sub

        Private Sub Summary_Compute(ByRef result As String)
            ' Set result to the desired field value
            If Me.AccomodationType IsNot Nothing Then
                If Me.AccomodationType.Equals("singlerm") Then

                    result = "Single Room - " + Me.PropertyHeader.PropertyName

                ElseIf Me.AccomodationType.Equals("doublerm") Then

                    result = "Double Room - " + Me.PropertyHeader.PropertyName

                ElseIf Me.AccomodationType.Equals("bedsitter") Then

                    result = "Bedsitter - " + Me.PropertyHeader.PropertyName

                ElseIf Me.AccomodationType.Equals("appart") Then

                    result = Me.UnitNo + " (" + Me.Bedrooms.ToString() + " Bedroom(s) Appartment - " + Me.PropertyHeader.PropertyName + ")"

                ElseIf Me.AccomodationType.Equals("bungalow") Then

                    result = Me.UnitNo + " (" + Me.Bedrooms.ToString() + " Bedroom(s) Bungalow - " + Me.PropertyHeader.PropertyName + ")"

                ElseIf Me.AccomodationType.Equals("mansionette") Then

                    result = Me.UnitNo + " (" + Me.Bedrooms.ToString() + " Bedroom(s) Mansionette - " + Me.PropertyHeader.PropertyName + ")"

                End If
            End If

        End Sub
    End Class

End Namespace
