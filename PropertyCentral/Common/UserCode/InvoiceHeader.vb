﻿
Namespace LightSwitchApplication

    Public Class InvoiceHeader

        Private Sub InvoiceTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            Dim total = 0.0

            For Each d In Me.InvoiceDetails
                total += d.ItemTotalCost
            Next
            result = total
        End Sub
    End Class

End Namespace
