﻿
Namespace LightSwitchApplication

    Public Class TenantDetail

        Private Sub TenantDetail_Created()

            Me.OccupancyDate = Date.Now().Date
            Me.RentStatus = "due"
            Me.RentDue = 0

        End Sub

        Private Sub ExitDate_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.ExitDate IsNot Nothing And Me.ExitDate < Me.OccupancyDate Then
                results.AddPropertyError("Date when tenant exits house cannot be less than occupation date")
            End If

        End Sub

        Private Sub OccupancyDate_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.OccupancyDate > Date.Today Then
                results.AddPropertyError("Occupation date cannot be in the future.")
            End If
        End Sub

        'Private Sub RentDue_Compute(ByRef result As Decimal)
        '    ' Set result to the desired field value
        '    Dim total = 0.0

        '    For Each d In Me.TenantPayments
        '        total += d.Amount
        '    Next
        '    result = total

        'End Sub
    End Class

End Namespace
