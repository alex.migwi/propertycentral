﻿Imports System.Net
Imports System.Net.Mail
Imports System.IO

Namespace LightSwitchApplication

    Public Module EmailHelper

        Public Function SendMail(emailSettings As EmailSetting, sendFrom As String, sendTo As String, subject As String, body As String)

            'Dim SMTPServer As String = emailSettings.SMTPServer
            'Dim SMTPUserId As String = emailSettings.SMTPUsername
            'Dim SMTPPassword As String = emailSettings.SMTPPassword
            'Dim SMTPPort As Integer = emailSettings.SMTPPort

            Dim fromAddress As New MailAddress(sendFrom)
            Dim toAddress As New MailAddress(sendTo)
            Dim mail As New MailMessage()

            mail.From = fromAddress
            mail.To.Add(toAddress)
            mail.Subject = subject
            mail.Body = body

            If body.ToLower().Contains("<html>") Then
                mail.IsBodyHtml = True
            End If

            Dim smtp As New SmtpClient(emailSettings.SMTPServer, emailSettings.SMTPPort)
            'If Attachment IsNot Nothing AndAlso Not String.IsNullOrEmpty(filename) Then
            '    Using ms As New MemoryStream(Attachment)
            '        mail.Attachments.Add(New Attachment(ms, filename))
            '        smtp.Send(mail)
            '    End Using
            'Else
            Try

                smtp.Send(mail)
                Return vbNullString

            Catch ex As Exception

                Return ex.ToString()

            End Try

            'End If

        End Function

    End Module

End Namespace
