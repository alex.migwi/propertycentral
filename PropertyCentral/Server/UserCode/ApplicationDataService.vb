﻿

Namespace LightSwitchApplication

    Public Class ApplicationDataService

        Private Sub TenantDetails_Updating(entity As TenantDetail)

            'create dataworkspace
            Dim dw = Me.DataWorkspace.ApplicationData

            Dim theProperty = entity.PropertyID
            If entity.ExitDate.HasValue Then
                Dim details = (From propdetails In dw.PropertyDetailHouses
                     Where propdetails.Id = entity.PropertyID).Execute
                'Set the house vacant when the exit date is set.
                For Each detail In details
                    detail.Availability = "vacant"
                    detail.FromDate = entity.ExitDate
                    'results.AddEntityResult(detail.Summary, ValidationSeverity.Informational)
                Next
            End If

        End Sub
        Private Sub TenantDetails_Validate(entity As TenantDetail, results As EntitySetValidationResultsBuilder)
            ' Check for validation errors for deletions
            Dim dw = Me.DataWorkspace.ApplicationData

            If entity.Details.EntityState = EntityState.Deleted Then
                'results.AddEntityResult(entity.Details.EntityState.ToString(), ValidationSeverity.Informational)
                If entity.RentStatus.Equals("due") Then
                    results.AddEntityError("Lease / Let record cannot deleted, there is an outstanding balance.")
                Else
                    Dim details = (From det In dw.PropertyDetailHouses Where det.Id = entity.PropertyID).Execute

                    For Each detail In details

                        detail.Availability = "vacant"
                        detail.FromDate = Now.Date()

                    Next

                End If
            End If

            If entity.Details.EntityState = EntityState.Added Then
                If entity.ExitDate.HasValue Then

                End If
            End If

        End Sub

        Private Sub TenantDetails_Deleting(entity As TenantDetail, results As EntitySetValidationResultsBuilder)
            ' Check for validation errors for deletions
            If entity.Details.ValidationResults.Errors.Any Then
                'Try
                'Throw New ValidationException(Nothing, Nothing, entity.Details.ValidationResults)
                'Catch ex As ValidationException
                results.AddEntityResult(entity.Details.ValidationResults.ToString, ValidationSeverity.Informational)
                'End Try

            End If

        End Sub


        Private Sub TenantDetails_Inserting(entity As TenantDetail, results As EntitySetValidationResultsBuilder)
            'Check for validation errors for deletions
            If entity.Details.ValidationResults.Errors.Any Then
                Try
                    Throw New ValidationException(Nothing, Nothing, entity.Details.ValidationResults)
                Catch ex As ValidationException
                    results.AddEntityResult(ex.ToString, ValidationSeverity.Informational)
                End Try

            End If
        End Sub

        Private Sub TenantDetails_Inserted(entity As TenantDetail)

            'Notify the Tenant by email about the new property they have occupied
            If entity.TenantHeader.Email IsNot Nothing Then

                WelcomeNewTenant(entity)
                
            End If

        End Sub

        Private Sub TenantDetails_Updated(entity As TenantDetail)


            If entity.Details.Properties.PropertyToLet.OriginalValue IsNot Nothing AndAlso
                entity.PropertyToLet IsNot Nothing AndAlso
                entity.ExitDate IsNot Nothing Then
                'Not (entity.Details.Properties.PropertyToLet.OriginalValue = "Closed") Then

                'Notify the Tenant by email about their exit from the house 
                If entity.TenantHeader.Email IsNot Nothing Then
                    'Dim emailSent = TenantExit(entity)
                    'If emailSent Then

                    'End If
                End If
            End If

        End Sub

        Private Sub WelcomeNewTenant(entity As TenantDetail)
            ' Send an email alert when a new tenant occupies a property
            'to both tenant and Landlord
            Dim dw = DataWorkspace.ApplicationData
            Dim agencyEmail As AgencyDetail = dw.AgencyDetails.FirstOrDefault()
            Dim emailSettings As EmailSetting = dw.EmailSettings.FirstOrDefault()

            'Set default sent success status as false
            'Dim sentStatus = False

            If agencyEmail.Email Is Nothing Then

                MsgBox("Failed to send email. Add email to agency details in settings.", MsgBoxStyle.Critical)

            Else
                Dim sendFrom = agencyEmail.Email
                Dim sendTo = entity.TenantHeader.Email
                Dim subject = "Welcome to " & entity.PropertyToLet
                Dim body = ""
                'get draft welcome message
                Dim emailMsgs = (From emails In dw.EmailDrafts
                     Where emails.DraftName = "Welcome Email").Execute
                'Set the email message.
                If (emailMsgs IsNot Nothing) Then

                    For Each emailMsg In emailMsgs
                        body = emailMsg.Message
                    Next
                Else
                    MsgBox("Click Yes to create a new draft Welcome Email Message. No cancels sending the email.", MsgBoxStyle.YesNo, "Create New Welcome Email Draft.")
                    If (MsgBoxResult.Yes) Then
                        'Open Screen to create new Draft Welcome Email
                    Else
                        'Abort sending the email
                        Exit Sub
                    End If
                End If

                'send the email
                Dim sendMsg As String = EmailHelper.SendMail(emailSettings, sendFrom, sendTo, subject, body)

                If (sendMsg IsNot Nothing) Then

                    'save the email to outbox
                    Using tempWorkspace As New DataWorkspace()
                        Dim newEmail = tempWorkspace.ApplicationData.OutBoxes.AddNew()
                        With newEmail
                            .Recipient = sendTo
                            .Sender = sendFrom
                            .Subject = subject
                            .Message = body
                        End With
                        Try
                            tempWorkspace.ApplicationData.SaveChanges()
                            ' If you want, you can write some code here to create a record in an audit table
                            MsgBox("An error occured and the email could not be sent. The message has been saved, go to Outbox to resend.", MsgBoxStyle.Information)
                        Catch ex As Exception
                            MsgBox("An error occured and the email could not be saved to Outbox.", MsgBoxStyle.Information)
                        End Try
                    End Using

                End If
            End If

        End Sub

        Private Sub PropertyDetailHouses_Updating(entity As PropertyDetailHouse)

            'Update related tenant rental details if the rent of the property has been adjusted
            '    Dim dw = Me.DataWorkspace.ApplicationData

            '    If entity.Details.Properties.Rent.OriginalValue > 0 AndAlso
            '        entity.Details.Properties.Rent.OriginalValue <> entity.Details.Properties.Rent.Value Then
            '        'Get the related tenant detail rows and update the rent payable

            '        Try
            '            'try to update the product price
            '            Dim currentDetails = (From td In dw.TenantDetails
            '                  Where td.PropertyID = entity.Id And td.ExitDate.HasValue = False).Execute

            '            For Each currentDetail In currentDetails
            '                currentDetail.RentPayable = entity.Rent
            '                '
            '            Next

            '        Catch
            '            MsgBox("The Rent for Tenant in " + entity.Summary + " was not updated. This record has to be updated manually.", MsgBoxStyle.Critical, "Tenant Rent Detail Update Error!")
            '        End Try

            '    End If

            '    dw.SaveChanges()

        End Sub

    End Class

End Namespace
